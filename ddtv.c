/*  ddtv.c - DD-ternary logic visualization utility

    Copyright (C) 2010-2011, Alexander A. Shabarshin <me@shaos.net>
    Copyright (C) 2015-2020, TERNARY RESEARCH CORPORATION <ddt@ternary.info>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifndef NOGIF
#include <gif_lib.h>
#endif
#include "nedofont.h"
#include "my_text.h"
#include "ddt.h"

#if 1
#define DEBUG
#endif
/* in this version we have limit for 256 functions with up to 255 inputs and 99 outputs */
#define FUNIBITS 8 /* number of bits to store function input index */
#define FUNOMULT 100 /* multiplier to store function output index */
#define VSPACE 3 /* vertical space between functions of the same level */
#define MAXLEV 100 /* maximal number of levels */
/* standard character codes in DOS cp866 */
#define S_V  0xB3
#define S_H  0xC4
#define S_LT 0xDA
#define S_RT 0xBF
#define S_LB 0xC0
#define S_RB 0xD9
#define S_VL 0xB4
#define S_VR 0xC3
#define S_VH 0xC5
/* non-standards character codes */
#define S_NT 0xF2
#define S_NL 0xF3
#define S_NR 0xF4
#define S_NB 0xF5
#define S_NA 0xF6
#define S_M1 0xF8
#define S_M2 0xF9
#define S_M3 0xFA
#define S_M4 0xFB
#define S_M5 0xFC
#define S_M6 0xFD
#define S_HS 0xFE

int n_fun = 0;
int n_inp = 0;
int n_tmp = 0;
int n_out = 0;
int n_lev = 0;
int m_nam = 0;
int m_nami = 0;
int m_namt = 0;
int m_namo = 0;

int l_num[MAXLEV]; /* number of elements in the level */
int l_inp[MAXLEV]; /* number of functions inputs */
int l_out[MAXLEV]; /* number of functions outputs */
int l_uin[MAXLEV]; /* number of unique functions inputs */
int l_cin[MAXLEV]; /* number of constant functions inputs */
int l_off[MAXLEV]; /* offset to the left side of the level */

int gW = 0;
int gH = 0;

typedef struct glob_el
{
  unsigned char c;  /* character code (extended DOS cp866) */
  unsigned char n;  /* input/output index that starts with 1 */
  unsigned short f; /* function identifier (if applicable) */
  signed long s; /* source identifier (>0 for temps, <0 for inputs) */
} GLOBEL;

GLOBEL **glob = NULL;

int createGlob(int w, int h);
int printGlob(int x, int y, unsigned char *s, int f);
int saveGlob(char *filename);
void destroyGlob(void);

#define printGlob0(x,y,s) printGlob(x,y,(unsigned char*)s,0)

int createGlob(int w, int h)
{
 int j;
 GLOBEL **g;
#ifdef DEBUG
 printf("createGlob(%i,%i)\n",w,h);
#endif
 if(gW>0 && gW!=w) return -1;
 if(gH > h) return -2;
 if(gH==h) return 0;
 g = (GLOBEL**)malloc(sizeof(char*)*(unsigned int)h);
 if(g==NULL) return -3;
 if(glob==NULL)
 {
    for(j=0;j<h;j++)
    {
      g[j] = (GLOBEL*)calloc((size_t)w,sizeof(GLOBEL));
      if(g[j]==NULL)
      {
         while(--j>=0)
         {
           free(g[j]);
         }
         free(g);
         glob = NULL;
         gW = gH = 0;
         return -4;
      }
    }
 }
 if(glob!=NULL)
 {
    free(glob);
    for(j=gH;j<h;j++)
    {
      g[j] = (GLOBEL*)calloc((size_t)w,sizeof(GLOBEL));
      if(g[j]==NULL)
      {
         while(--j>=0)
         {
           free(g[j]);
         }
         free(g);
         glob = NULL;
         gW = gH = 0;
         return -5;
      }
    }
 }
 glob = g;
 gW = w;
 gH = h;
 return 0;
}

int printGlob(int x, int y, unsigned char *s, int f)
{
 int i,j,k,l;
 GLOBEL* g;
#ifdef DEBUG
 printf("printGlob(%i,%i,%s,%i)\n",x,y,s,f);
#endif
 if(y<0) return -1;
 l = (int)strlen((char*)s);
 if(y>=gH)
 {
   k = createGlob(gW,y+1);
   if(k) return -10+k;
 }
 g = glob[y];
 i = 0;
 while(s[i])
 {
   j = x + i;
   if(j>=0 && j<gW)
   {
     g[j].c = s[i];
     g[j].f = (unsigned short)f;
   }
   i++;
 }
 return 0;
}

int saveGlob(char *filename)
{
 int i,j,k;
 FILE *f;
 GLOBEL *g;
#ifdef DEBUG
 printf("saveGlob(%s)\n",filename);
#endif
 /* fix first */
 for(j=0;j<gH;j++)
 {
   g = glob[j];
   k = -1;
   for(i=0;i<gW;i++)
     if(g[i].c) k = i;
   for(i=0;i<k;i++)
     if(!g[i].c) g[i].c = ' ';
 }
 /* then save */
 f = fopen(filename,"wt");
 if(f==NULL) return -1;
 for(j=0;j<gH;j++)
 {
   g = glob[j];
   i = 0;
   while(g[i].c) fputc(g[i++].c,f);
   fputc('\n',f);
 }
 fclose(f);
 return 0;
}

void destroyGlob(void)
{
 int j;
#ifdef DEBUG
 printf("destroyGlob()\n");
#endif
 for(j=0;j<gH;j++) free(glob[j]);
 free(glob);
 glob = NULL;
 gH = gW = 0;
}

int main(int argc, char **argv)
{
 unsigned long sz;
 int i,j,k,n,q,u,x,y,z;
#ifndef NOGIF
 int f_gif = 1;
#endif
 int f_inside = 0;
 char str[MY_MAXLEN],st1[256],st2[16],fname[100],*po,*p1,*p2,*p3,*p4;
 FILE *f;
 Line *l,*l1,*l2;
 Text *t,*v,*w;
 GLOBEL* g;
 printf(DDT_COPYR "DDTv converts C-source of DDT-scheme to GIF image\n\n");
#if 1
 if(argc < 2)
 {
    printf("Usage:\n\tddtv ddt_file.c\n\n");
    return -1;
 }
 strncpy(fname,argv[1],99);
 fname[99] = 0;
#else
 strcpy(fname,"ddt_sum.c");
#endif
 sz = strlen(fname);
 if(strncmp(fname,"ddt_",4) || fname[sz-1]!='c' || fname[sz-2]!='.')
 {
    printf("ERROR: File %s is not DDT-solution file!\n\n",fname);
    return -2;
 }
 f = fopen(fname,"rt");
 if(f==NULL)
 {
    printf("ERROR: File %s can not be opened!\n\n",fname);
    return -3;
 }
 printf("Reading '%s'...\n",fname);
 po = strrchr(fname,'.');
 if(po!=NULL) *po = 0;
 v = TextNew();
 t = TextNew();
 while(1)
 {
    fgets(str,MY_MAXLEN,f);
    if(feof(f)) break;
    po = strrchr(str,'\n');
    if(po!=NULL) *po=0;
    po = strrchr(str,'\r');
    if(po!=NULL) *po=0;
    po = str;
    while(*po==' '||*po=='\t') po++;
    if(*po=='{') f_inside++;
    else if(*po=='}') f_inside--;
    else if(!strncmp(po,"int ",4) && !f_inside)
    {
#ifdef DEBUG
       printf("%s\n",po);
#endif
       sz = strlen(po);
       for(i=0;i<(int)sz;i++)
       {
         if(i>=3&&po[i-3]=='D'&&po[i-2]=='D'&&po[i-1]=='T'&&po[i]==' ') 
         {
           n_inp++;
           p2 = &po[i];
           while(*p2==' '||*p2=='\t') p2++;
           strncpy(st2,p2,15);
           st2[15] = 0;
           p3 = strchr(st2,',');
           if(p3==NULL) printf("ERROR: bad function definition:\n\t%s\n",po);
           else
           {
             *p3 = 0;
             while(p3--!=st2) *p3=(char)toupper(*p3);
             l = TextAdd(v,st2);
             l->type = 10;
             l->adr = (unsigned short)n_inp;
           }
         }
       }
    }
    else if(f_inside)
    {
     y = -1;
     if(strstr(po,"ddt_")) y = 1;
     else if(strchr(po,'*')) y = 0;
     if(y >= 0)
     {
#ifdef DEBUG
       printf("%s\n",po);
#endif
       sz = strlen(po);
       for(i=0;i<(int)sz;i++)
       {
         if(po[i]==';')
         {
            po[i] = 0;
            break;
         }
         if(i>=2&&po[i]==','&&po[i-1]=='F'&&po[i-2]=='(')
         {
            po[i-2] = ' ';
            po[i-1] = ' ';
            po[i] = '(';
         }
         else po[i] = (char)toupper(po[i]);
       }
       if(y==1) /* function */
       {
         n_fun++;
         strncpy(st1,po,255);
         st1[255] = 0;
         p2 = st1;
         while(1)
         {
           p3 = strchr(p2,'&');
           if(p3==NULL) break;
           p3++;
           p4 = strchr(p3,',');
           if(p4==NULL)
           {
             p4 = strchr(p3,')');
           }
           if(p4==NULL) break;
           *p4 = 0;
           l = TextFindFirstStr(v,p3);
           if(l!=NULL) printf("ERROR: multiple usage of temporary variable %s\n",p3);
           else
           {
             n_tmp++;
             l = TextAdd(v,p3);
             l->type = 11;
             l->adr = (unsigned short)n_tmp;
             y = l->adr;
           }
           p2 = p4+1;
         }
       }
       else /* output */
       {
         p2 = strchr(po,'*');
         if(p2!=NULL) po=p2+1;
         n_out++;
         strncpy(st2,po,15);
         st2[15] = 0;
         p3 = strchr(st2,' ');
         if(p3!=NULL) *p3=0;
         l = TextAdd(v,st2);
         l->type = 12;
         l->adr = (unsigned short)n_out;
       }
       l = TextAdd(t,po);
       l->type = y;
       if(y==0) l->id = n_out;
     }
    }
 }
 fclose(f);
 printf("%i functions found\n",n_fun);
 printf("%i input variables found\n",n_inp);
 printf("%i temporary variables found\n",n_tmp);
 printf("%i output variables found\n",n_out);
#ifdef DEBUG
 printf("\nText:\n");
 TextList(t);
 printf("\n");
#endif
 printf("Tracing links...\n");

/*

  Structure of list of variables (v):
  str : name of variable in upper case
  type=10 : scheme inputs
  type=11 : temporary vars (function output)
  type=12 : scheme outputs
  type=13 : function inputs (calculated below)
  id2=-1 : variable is connected to ternary constant N
  id2=0 : variable is connected to ternary constant O
  id2=1 : variable is connected to ternary constant P
  id2=2 : variable is connected to function output with combined index id (applicable only to temporary variables)
  id2=10 : variable is connected to scheme input with index id
  id2=11 : variable is connected to temporary variable with index id
  adr : index of variable of this type (in case of a function input is combined function index and input index)
  len : level (0 means inputs, 1 means connection to inputs or constants etc)

  Structure of list of functions (t):
  type=0 : output
  type=N : function N
  id : coordinate y
  id2 : coordinate x
  adr : combined (inputs<<4)|outputs (special cases with 1 output: #03FF for MEM, #03FE for E12, #03FD for E21, #04FC for MUX)
  len : level (1,2,etc)

*/

 for(l=t->first;l!=NULL;l=l->next)
 {
   if(l->type==0) /* output */
   {
     strncpy(st1,l->str,255);
     st1[255] = 0;
#ifdef DEBUG
     printf("%s\n",st1);
#endif
     po = strchr(st1,'=');
     if(po==NULL) printf("ERROR: wrong output expression %s\n",l->str);
     else
     {
       *po = 0;
       p2 = po-1;
       po++;
       if(*p2==' ') *p2=0;
       while(*po==' ') po++;
       l2 = TextFindFirstStr(v,po);
       if(l2==NULL) printf("ERROR: can't find variable %s\n",po);
       else
       {
         j = l2->adr;
         y = l2->type;
         if(y!=10 && y!=11) printf("ERROR: variable %s is not valid for connection\n",po);
         else
         {
           l2 = TextFindFirstStr(v,st1);
           if(l2==NULL) printf("ERROR: can't find variable %s\n",st1);
           else
           {
             if(l2->type!=12) printf("ERROR: variable %s is not an output\n",st1);
             else
             {
               i = l2->adr;
               l2->id2 = y;
               l2->id = j;
#ifdef DEBUG
               printf("output %s (%i) is connected to %s variable %s (%i)\n",st1,i,(y==10)?"input":"temporary",po,j);
#endif
             }
           }
         }
       }
     }
   }
   else /* function */
   {
     z = l->type;
     strncpy(st1,l->str,255);
     st1[255] = 0;
#ifdef DEBUG
     printf("%s\n",st1);
#endif
     po = strchr(st1,'=');
     if(po==NULL) printf("ERROR: wrong function call %s\n",l->str);
     else
     {
       *po = 0;
       p2 = po-1;
       po++;
       if(*p2==' ') *p2=0;
       p1 = strstr(po,"DDT_");
       p3 = strchr(po,'(');
       p4 = strchr(po,')');
       if(p1==NULL || p3==NULL || p4==NULL) printf("ERROR: wrong function call %s\n",l->str);
       else
       {
          p1 = p1+4;
          po = strchr(p1,' ');
          if(po!=NULL) *po=0;
          k = (int)strlen(p1);
          if(k==3)
          {
             if(p1[0]=='M')
             {
                if(p1[1]=='E' && p1[2]=='M') l->adr = 0xFE; /* 0xFF */
                if(p1[1]=='U' && p1[2]=='X') l->adr = 0xFB; /* 0xFC */
             }
             if(p1[0]=='E')
             {
                if(p1[1]=='1' && p1[2]=='2') l->adr = 0xFD; /* 0xFE */
                if(p1[1]=='2' && p1[2]=='1') l->adr = 0xFC; /* 0xFD */
             }
          }
          if(k > m_nam) m_nam = k;
          po = p3+1;
          *p4 = 0;
          p2 = strtok(po,",");
          i = k = 0;
          while(p2!=NULL)
          {
            i++;
            if(*p2=='&')
            {
              p2++;
              if(k==0) i = 1;
              k = 1;
            }
            if(!k) /* function inputs */
            {
              l->adr += 0x100;
              if(i>=(1<<FUNIBITS))
              {
                printf("ERROR: function %s has too many inputs\n",st1);
                break;
              }
              sprintf(st2,"%s.%i",st1,i);
              l1 = TextAdd(v,st2);
              l1->type = 13;
              l1->adr = (unsigned short)((z<<FUNIBITS)|i);
              if(!p2[1])
              {
                y = -999;
                switch(*p2)
                {
                  case 'N': y = -1; break;
                  case 'O': y =  0; break;
                  case 'P': y =  1; break;
                  case '0': y = 0; break;
                  case '1': y = 1; break;
                  default: printf("ERROR: unknown constant %s in %s\n",p2,l->str);
                }
                if(y >= -1)
                {
                  l1->id2 = y;
#ifdef DEBUG
                  printf("input %i of function %s (%i) is connected to constant %s (%i)\n",i,p1,z,p2,y);
#endif
                }
              }
              else
              {
                l2 = TextFindFirstStr(v,p2);
                if(l2==NULL) printf("ERROR: can't find variable %s\n",p2);
                else
                {
                  j = l2->adr;
                  y = l2->type;
                  if(y!=10 && y!=11) printf("ERROR: variable %s is not valid for connection\n",p2);
                  else
                  {
                    l1->id2 = y;
                    l1->id = j;
#ifdef DEBUG
                    printf("input %i of function %s (%i) is connected to %s variable %s (%i)\n",i,p1,z,(y==10)?"input":"temporary",p2,j);
#endif
                  }
                }
              }
            }
            else /* function outputs */
            {
              l->adr++;
              if(i>=FUNOMULT)
              {
                printf("ERROR: function %s has too many outputs\n",st1);
                break;
              }
              l2 = TextFindFirstStr(v,p2);
              if(l2==NULL) printf("ERROR: can't find variable %s\n",p2);
              else
              {
                j = l2->adr;
                y = l2->type;
                if(y!=11) printf("ERROR: variable %s is not valid for connection to function output\n",p2);
                else
                {
                    l2->id2 = 2;
                    l2->id = FUNOMULT*z+i;
#ifdef DEBUG
                    printf("output %i of function %s (%i) is connected to temporary variable %s (%i)\n",i,p1,z,p2,j);
#endif
                }
              }
            }
            p2 = strtok(NULL,",");
          }
       }
     }
   }
 }
 printf("%li links traced\n",v->num);
 printf("Detecting levels...\n");
 y = 0;
 while(1)
 {
   k = 1;
   for(l=v->first;l!=NULL;l=l->next)
   {
     if(l->type!=10 && l->len==0)
     {
       k = 0;
       if(l->id2<=1) l->len = 1;
       else
       {
          if(l->id2==2)
          {
            x = 0;
            z = l->id/100;
            for(l2=v->first;l2!=NULL;l2=l2->next)
            {
              if(l2->type==13 && (l2->adr>>FUNIBITS)==z)
              {
                if(l2->len==0)
                {
                  x = 0;
                  break;
                }
                if(l2->len > x) x=l2->len;
              }
            }
            if(x > 0)
            {
              l->len = (unsigned short)x;
              if(l->len > y) y = l->len;
              l1 = TextFindFirstTyp(t,z);
              if(l1==NULL) printf("ERROR: can't find function %i\n",z);
              else l1->len = l->len;
            }
          }
          else
          {
            for(l2=v->first;l2!=NULL;l2=l2->next)
            {
              if(l2->type==l->id2 && l2->adr==l->id) break;
            }
            if(l2==NULL) printf("ERROR: can't find source %i/%i",l->id2,l->id);
            else
            {
              if(l2->len || l->id2==10)
              {
                 l->len = (unsigned short)(l2->len+1);
                 if(l->len > y) y = l->len;
                 if(l->type==12)
                 {
                   l1 = TextFindFirstTypId(t,0,l->adr);
                   if(l1==NULL) printf("ERROR: can't find output %i\n",l->adr);
                   else l1->len = l->len;
                 }
              }
            }
          }
       }
     }
   }
   if(k) break;
 }
 n_lev = y;
 printf("%i levels detected (%i internal)\n",n_lev,n_lev-1);
#ifdef DEBUG
 printf("\nVars:\n");
 TextList(v);
 printf("\nText:\n");
 TextList(t);
 printf("\n");
#endif
 w = TextNew();
 printf("Level 0: %i external inputs\n",n_inp);
 l_num[0] = n_inp;
 for(i=1;i<n_lev;i++)
 {
   TextAllDelete(w);
   q = x = y = z = u = 0;
   l = TextFindFirstLen(t,(unsigned short)i);
   while(l!=NULL)
   {
     if(l->type != 0)
     {
       po = l->str;
       j = 0;
       k = 1;
       while(*po)
       {
         if(*po==',') k++;
         if(*po=='&') j++;
         po++;
       }
       z++;
       k -= j;
       x += k;
       y += j;
#ifdef DEBUG
       printf("Level %i function %s -> %i/%i\n",i,l->str,k,j);
#endif
       l2 = TextFindFirstTyp(v,13);
       while(l2!=NULL)
       {
        if((l2->adr>>FUNIBITS)==l->type)
        {
         if(l2->id2<=1) q++;
         else
         {
          sprintf(st1,"%i/%i",l2->id2,l2->id);
          l1 = TextFindFirstStr(w,st1);
          if(l1==NULL)
          {
            l1 = TextAdd(w,st1);
            u++;
          }
         }
        }
        l2 = TextFindNext(v);
       }
     }
     l = TextFindNext(t);
   }
   printf("Level %i: %i functions with %i inputs (u=%i c=%i) and %i outputs\n",i,z,x,u,q,y);
   l_num[i] = z;
   l_inp[i] = x;
   l_out[i] = y;
   l_uin[i] = u;
   l_cin[i] = q;
 }
 printf("Level %i: %i external outputs\n",i,n_out);
 l_num[i] = n_out;
 TextAllDelete(w);
 for(l=v->first;l!=NULL;l=l->next)
 {
   sz = strlen(l->str);
   if(l->type==10 && (int)sz>m_nami) m_nami=(int)sz;
   if(l->type==11 && (int)sz>m_namt) m_namt=(int)sz;
   if(l->type==12 && (int)sz>m_namo) m_namo=(int)sz;
 }
 printf("Longest function name is %i characters\n",m_nam);
 printf("Longest input name is %i characters\n",m_nami);
 printf("Longest output name is %i characters\n",m_namo);
 printf("Longest temporary name is %i characters\n",m_namt);
 if(n_inp > n_out)
   k = n_inp;
 else
   k = n_out;
#ifdef DEBUG
 printf("Offset to inputs is 1 cell\n");
#endif
 l_off[0] = 1 + m_nami; /* zero level for inputs */
 x = l_off[0] + n_inp;
 y = 0;
 for(i=1;i<n_lev;i++)
 {
#ifdef DEBUG
   printf("Offset to level %i is %i cells\n",i,x);
#endif
   l_off[i] = x + l_uin[i]; /* offset to the left side of the blocks */
   x += m_nam + 3 + l_uin[i] + l_out[i] + m_namt;
   if(l_cin[i]) x += 2; /* constants add 2 more columns */
   j = 3 + k + 3*l_num[i];
   if(l_inp[i] > l_out[i])
      j += l_inp[i];
   else
      j += l_out[i];
   if(j > y) y = j;
 }
#ifdef DEBUG
 printf("Offset to outputs is %i cells\n",x);
#endif
 l_off[i] = x; /* last level for outputs */
 x += n_out + m_namo + 1;
 printf("Initial size of the image %i x %i (%i x %i cells)\n",x<<3,y<<3,x,y);
 printf("Sorting by level...\n");
 k = createGlob(x,y);
 if(k) printf("ERROR: can't create image (%i)\n",k);
 else
 {
   TextSort(v,TextFldLen);
   TextSort(t,TextFldLen);
   for(i=1;i<=n_inp;i++)
   {
      l = TextFindFirstTyp(v,10);
      while(l!=NULL)
      {
        if(l->adr==i)
        {
          sz = strlen(l->str);
          printGlob0(1+m_nami-(int)sz,i,l->str);
          g = &glob[i][1+m_nami];
          for(j=0;j<=n_inp-i;j++)
          {
            if(j==0) g->c = S_HS;
            else g->c = S_H;
            g->s = -i;
            g++;
          }
          break;
        }
        l = TextFindNext(v);
      }
      if(l==NULL) printf("ERROR: can't find input %i\n",i);
   }
   for(i=1;i<=n_out;i++)
   {
      l = TextFindFirstTyp(v,12);
      while(l!=NULL)
      {
        if(l->adr==i)
        {
          printGlob0(x-1-m_namo,i,l->str);
          k = x - 2 - m_namo;
          g = &glob[i][k];
          for(j=0;j<=n_out-i;j++)
          {
            g->c = S_H;
            g--;
            k--;
          }
          k++;
          l = TextFindFirstTypId(t,0,i);
          if(l==NULL) printf("ERROR: can't find output %i in text\n",i);
          else l->id2 = k;
          break;
        }
        l = TextFindNext(v);
      }
      if(l==NULL) printf("ERROR: can't find output %i\n",i);
   }




   saveGlob(fname);
#ifndef NOGIF
   if(f_gif)
   {
     GifFileType *GifFile;
     GifByteType *Ptr;
     ColorMapObject *colormap;
     GifColorType ScratchMap[4];
     for(i=0;i<4;i++)
     {
       switch(i)
       {
         case 0: k = 0xFF; break;
         case 1: k = 0xC0; break;
         case 2: k = 0x80; break;
         case 3: k = 0x00; break;
       }
       ScratchMap[i].Red = (GifByteType)k;
       ScratchMap[i].Green = (GifByteType)k;
       ScratchMap[i].Blue = (GifByteType)k;
     }
     colormap = GifMakeMapObject(4, ScratchMap);
     Ptr = (GifByteType*)calloc((size_t)(x<<3),sizeof(GifByteType));
     if(Ptr==NULL) printf("ERROR: can't allocate enough memory for GIF\n");
     else
     {
       strcat(fname,".gif");
       GifFile = EGifOpenFileName(fname,0,NULL);
       if(GifFile!=NULL &&
          EGifPutScreenDesc(GifFile,x<<3,y<<3,2,0,colormap)!=GIF_ERROR &&
          EGifPutImageDesc(GifFile,0,0,x<<3,y<<3,0,NULL)!=GIF_ERROR)
       {
          for(j=0;j<(y<<3);j++)
          {
             for(i=0;i<x;i++)
             {
                u = glob[j>>3][i].c;
                if(!u) u = ' ';
                k = font8x8[u-FONT8X8_FIRST][j&7];
                for(n=0;n<8;n++)
                {
                   if(k&0x80)
                   {
                      if(u<0x80) Ptr[(i<<3)+n]=2;
                      else Ptr[(i<<3)+n]=3;
                   }
                   else Ptr[(i<<3)+n]=0;
                   k<<=1;
                }
             }
             if(EGifPutLine(GifFile, Ptr, x<<3)==GIF_ERROR)
             {
                printf("ERROR: can't write line %i to GIF file\n",j);
                break;
             }
          }
       }
       if(GifFile!=NULL && EGifCloseFile(GifFile,NULL)==GIF_ERROR)
       {
          printf("ERROR: can't properly close GIF file\n");
       }
       free(Ptr);
     }
   }
#endif
 }
#ifdef DEBUG
 printf("\nVars:\n");
 TextList(v);
 printf("\nText:\n");
 TextList(t);
 printf("\n");
#endif
 destroyGlob();
 TextDel(t);
 TextDel(v);
 TextDel(w);
 return 0;
}
