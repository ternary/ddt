# DDT (Decision Diagrams for Ternary) synthesis and simulation tools

This package was created in 2010 to automate design of ternary circuits,
based on analog switches DG403. Ternary signal may be connected to positive
voltage (+1 or P), negative voltage (-1 or N) or ground (0 or O). By
connecting logic voltage pins of DG403 (VL and GND) to upper or lower
part of full range of analog voltage (between V- and V+) we may get two
different types of basic ternary elements - E12 (VL=Ground, GND=Negative)
and E21 (VL=Positive, GND=Ground). Each DG403 may have two E12 or two E21
elements. One E12 and one E21 may be connected together to create ternary
multiplexer (MUX) or universal ternary element. Also some special
functions may be constructed from E12 and E21, namely BUF (ternary buffer),
INV (ternary inverter), BLN (block negative), BLP (block positive),
ROU (rotate up), ROD (rotated down), SHU (shift up), SHD (shift down),
PTI (positive ternary inverter), NTI (negative ternary inverter),
PHI (positive half inverter) and NHI (negative half inverter).

Source code ddt.h and ddt.c are set of functions for simulation of ternary
schematics programmatically in C-language.

DDTc is synthesis utility that may use command line argument as truth
table or as filename of DDT-file with truth table inside. Output of this
program is C-source that may be used for compiling simulation binary.

DDTp is permutating utility that is trying to find better solution
(in terms of DG403 number) by permutating inputs of given DDT-file and
calling DDTc for every combination with calculating of elements.

For more info see http://Ternary.info/wiki/index.php?n=Main.DDT

Copyright (c) 2010 Alexander A. Shabarshin <me@shaos.net>

Copyright (c) 2015 TERNARY RESEARCH CORPORATION <ddt@ternary.info>
