/*  ddt.c - decision diagram for ternary synthesis and simulation

    Copyright (C) 2010, Alexander A. Shabarshin <me@shaos.net>
    Copyright (C) 2015, TERNARY RESEARCH CORPORATION <ddt@ternary.info>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ddt.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char global_copyr[] = DDT_COPYR;

int ddt_error(DDT a)
{
 if(a!=N && a!=O && a!=P) return 1;
 return 0;
}

/* Basic Functions: */

int ddt_e12(int f, DDT c, DDT a, DDT b, DDT* r)
{
 if(f==DDT_SIM)
 {
   if(ddt_error(c)||ddt_error(a)||ddt_error(b)) return DDT_ERROR_ARG;
   if(r)
   {
     switch(c)
     {
       case N: *r=a; break;
       case O: *r=b; break;
       case P: *r=b; break;
     }
   }
   return 1;
 }
 if(f==DDT_E12) return 1;
 return 0;
}

int ddt_e21(int f, DDT c, DDT a, DDT b, DDT* r)
{
 if(f==DDT_SIM)
 {
   if(ddt_error(c)||ddt_error(a)||ddt_error(b)) return DDT_ERROR_ARG;
   if(r)
   {
     switch(c)
     {
       case N: *r=a; break;
       case O: *r=a; break;
       case P: *r=b; break;
     }
   }
   return 1;
 }
 if(f==DDT_E21) return 1;
 return 0;
}

int ddt_mem(int f, DDT c, DDT a, DDT b, DDT* r, int i)
{
 static int ddt_mem_size = 0;
 static char *ddt_mem_array = NULL;
 if(f==DDT_SIM)
 {
   if(ddt_error(c)||ddt_error(a)||ddt_error(b)||i<0) return DDT_ERROR_ARG;
   if(!ddt_mem_array)
   {
     ddt_mem_array = (char*)malloc(100);
     if(!ddt_mem_array) return DDT_ERROR_MEM;
     ddt_mem_size = 100;
     memset(ddt_mem_array,O,100);
   }
   if(i >= ddt_mem_size)
   {
     ddt_mem_array = (char*)realloc(ddt_mem_array,(size_t)ddt_mem_size+100);
     if(!ddt_mem_array) return DDT_ERROR_MEM;
     memset(&ddt_mem_array[ddt_mem_size],O,100);
     ddt_mem_size += 100;
   }
   switch(c)
   {
     case N: ddt_mem_array[i]=(char)a; break;
     case O: break;
     case P: ddt_mem_array[i]=(char)b; break;
   }
   if(r) *r=ddt_mem_array[i]; 
   return 1;
 }
 if(f==DDT_MEM) return 1;
 return 0;
}

/* Constructed Functions: */

int ddt_mux(int f, DDT c, DDT n, DDT o, DDT p, DDT *r)
{
 DDT r1,r2;
 int f1,f2;
 if(f==DDT_MUX) return 1;
 f1 = ddt_e21(f,c,o,p,&r1);
 if(f1 < 0) return f1;
 f2 = ddt_e12(f,c,n,r1,&r2);
 if(f2 < 0) return f2;
 if(r) *r = r2;
 return f1 + f2;
}

int ddt_buf(int f, DDT a, DDT* r)
{
 if(f==DDT_BUF) return 1;
 return ddt_mux(f,a,N,O,P,r);
}

int ddt_inv(int f, DDT a, DDT* r)
{
 if(f==DDT_INV) return 1;
 return ddt_mux(f,a,P,O,N,r);
}

int ddt_blp(int f, DDT a, DDT* r)
{
 if(f==DDT_BLP) return 1;
 return ddt_e12(f,a,N,O,r);
}

int ddt_bln(int f, DDT a, DDT* r)
{
 if(f==DDT_BLN) return 1;
 return ddt_e21(f,a,O,P,r);
}

int ddt_rou(int f, DDT a, DDT* r)
{
 if(f==DDT_ROU) return 1;
 return ddt_mux(f,a,O,P,N,r);
}

int ddt_rod(int f, DDT a, DDT* r)
{
 if(f==DDT_ROD) return 1;
 return ddt_mux(f,a,P,N,O,r);
}

int ddt_shu(int f, DDT a, DDT* r)
{
 if(f==DDT_SHU) return 1;
 return ddt_e12(f,a,O,P,r);
}

int ddt_shd(int f, DDT a, DDT* r)
{
 if(f==DDT_SHD) return 1;
 return ddt_e21(f,a,N,O,r);
}

int ddt_pti(int f, DDT a, DDT* r)
{
 if(f==DDT_PTI) return 1;
 return ddt_e21(f,a,P,N,r);
}

int ddt_nti(int f, DDT a, DDT* r)
{
 if(f==DDT_NTI) return 1;
 return ddt_e12(f,a,P,N,r);
}

int ddt_phi(int f, DDT a, DDT* r)
{
 if(f==DDT_PHI) return 1;
 return ddt_e21(f,a,P,O,r);
}

int ddt_nhi(int f, DDT a, DDT* r)
{
 if(f==DDT_NHI) return 1;
 return ddt_e12(f,a,O,N,r);
}

int ddt_min(int f, DDT a, DDT b, DDT *r)
{
 DDT r1,r2;
 int f1,f2;
 if(f==DDT_MIN) return 1;
 f1 = ddt_e12(f,a,N,O,&r1);
 if(f1 < 0) return f1;
 f2 = ddt_mux(f,b,N,r1,a,&r2);
 if(f2 < 0) return f2;
 if(r) *r = r2;
 return f1 + f2;
}

int ddt_max(int f, DDT a, DDT b, DDT *r)
{
 DDT r1,r2;
 int f1,f2;
 if(f==DDT_MAX) return 1;
 f1 = ddt_e21(f,a,O,P,&r1);
 if(f1 < 0) return f1;
 f2 = ddt_mux(f,b,a,r1,P,&r2);
 if(f2 < 0) return f2;
 if(r) *r = r2;
 return f1 + f2;
}
