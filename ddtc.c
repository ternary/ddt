/*  ddtc.c - DD-ternary logic synthesis tool and translator to C

    Copyright (C) 2010, Alexander A. Shabarshin <me@shaos.net>
    Copyright (C) 2015, TERNARY RESEARCH CORPORATION <ddt@ternary.info>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "my_text.h"
#include "ddt.h"

#if 0
#define DEBUG
#endif
#if 0
#define DEBUG_
#endif

int ddt_ins = 0;
int ddt_outs = 0;

int ddt_eflag = 0;

typedef struct _ddt_struct
{
 char *s;
 int a; 
}ddt_struct;

ddt_struct *ddt_sign;

Text* funs = NULL;

/* ddt_es > 0 if number of E21 is more than number of E12 */
/* ddt_es < 0 if number of E12 is more than number of E21 */

int ddt_es = 0;

/* Take 3 characters from tri, check, optimize and save it to tro */

int ddt_check(char *tri, char *tro)
{
  int i,l='O',x=0;
#ifdef DEBUG_
  printf("ddt_check %s\n",tri?tri:"NULL");
#endif
  if(tri==NULL || tro==NULL || strlen(tri)<3) return 0;
  for(i=0;i<3;i++) 
  {
    tri[i] = (char)toupper(tri[i]);
    if(tri[i]!='N' && tri[i]!='O' && tri[i]!='P' && 
       tri[i]!='0' && tri[i]!='1' && tri[i]!='X' &&
       tri[i]!='2' && tri[i]!='+' && tri[i]!='-'
      ) return 0;
    if(tri[i]=='0') tri[i] = 'O';
    if(tri[i]=='1' || tri[i]=='+') tri[i] = 'P';
    if(tri[i]=='2' || tri[i]=='-') tri[i] = 'N';
    if(tri[i]=='X') x++;
    else l = tri[i];
    tro[i] = tri[i];
  }
  tro[i] = 0;
#ifdef DEBUG
  printf("ddt_check : x=%i l=%c %s\n",x,l,tro);
#endif
  switch(x)
  {
    case 0: 
      x = -1;
      break;
    case 1:
      if(tro[0]=='X')
      {
        tro[0] = tro[1];
      }
      else if(tro[1]=='X')
      {
        if(ddt_es > 0)
          tro[1] = tro[2];
        else
          tro[1] = tro[0];
      }
      else /* tro[2]=='X' */
      {
        tro[2] = tro[1];
      }
      break;
    case 2:
      for(i=0;i<3;i++)
      {
        if(tro[i]=='X') tro[i]=(char)l;
      }
      break;
    case 3:
      strcpy(tro,"OOO");
      break;
  }
  if(tro[0]==tro[1] && tro[1]!=tro[2]) ddt_es++;
  if(tro[0]!=tro[1] && tro[1]==tro[2]) ddt_es--;
#ifdef DEBUG
  printf("ddt_check -> %i %s (%i)\n",x,tro,ddt_es);
#endif
  return x;
}

/* Check signature string, optimize and return allocated string with result */

char* ddt_new(char *s)
{
  int l,i=0,e=0;
  char *r;
#ifdef DEBUG_
  printf("ddt_new %s\n",s?s:"NULL");
#endif
  if(s==NULL) return NULL;
  l = (int)strlen(s)+1;
  r = (char*)malloc((size_t)l);
  if(r!=NULL)
  {
   while(s[i])
   {
    if(!ddt_check(&s[i],&r[i])){e++;break;}
    if(!s[++i]){e++;break;}
    if(!s[++i]){e++;break;}
    i++;
   }
   r[i] = 0;
   if(e)
   {
    free(r);
    r = NULL;
   }
  }
#ifdef DEBUG
  else l=0;
  printf("ddt_new -> %i (%i)\n",l,e);
#endif
  return r;
}

/* Calculate number of ternary arguments based on signature string */

int ddt_calc(char *s)
{
  int k=3,l,a=0;
#ifdef DEBUG_
  printf("ddt_calc %s\n",s?s:"NULL");
#endif
  if(s!=NULL)
  {
    a++;
    l = (int)strlen(s);
    while(k < l)
    {
      if(k!=l)
      {
        k*=3;
        a++;
      }
    }
    if(k > l) a=0;
  }
#ifdef DEBUG
  printf("ddt_calc -> %i\n",a);
#endif
  return a;
}

/* Choose function to implement based on first 3 characters of signature */

int ddt_func(char *s)
{
  int f=DDT_MUX;
#ifdef DEBUG_
  printf("ddt_func %s\n",s);
#endif
  if(!strncmp(s,"NOP",3)) f=0; /* output = input */
  else if(!ddt_eflag && !strncmp(s,"PON",3)) f=DDT_INV;
  else if(!ddt_eflag && !strncmp(s,"NOO",3)) f=DDT_BLP;
  else if(!ddt_eflag && !strncmp(s,"OOP",3)) f=DDT_BLN;
  else if(!ddt_eflag && !strncmp(s,"OPN",3)) f=DDT_ROU;
  else if(!ddt_eflag && !strncmp(s,"PNO",3)) f=DDT_ROD;
  else if(!ddt_eflag && !strncmp(s,"OPP",3)) f=DDT_SHU;
  else if(!ddt_eflag && !strncmp(s,"NNO",3)) f=DDT_SHD;
  else if(!ddt_eflag && !strncmp(s,"PPN",3)) f=DDT_PTI;
  else if(!ddt_eflag && !strncmp(s,"PNN",3)) f=DDT_NTI;
  else if(!ddt_eflag && !strncmp(s,"PPO",3)) f=DDT_PHI;
  else if(!ddt_eflag && !strncmp(s,"ONN",3)) f=DDT_NHI;
  else if(s[0]==s[1] && s[1]==s[2]) f=s[0]; /* 'N' or 'O' or 'P' */
  else if(s[0]!=s[1] && s[1]==s[2]) f=DDT_E12;
  else if(s[0]==s[1] && s[1]!=s[2]) f=DDT_E21;
#ifdef DEBUG
  printf("ddt_func -> %i (%c%c%c)\n",f,s[0],s[1],s[2]);
#endif
  return f;
}

/* Choose function to implement for level l>1 based on preprocessed signature string */

int ddt_fun(int l, char *s)
{
  int f=DDT_MUX;
  int c,i,t=1,r=1;
  char s3[4]="";
#ifdef DEBUG_
  printf("ddt_fun %i [%s]\n",l,s);
#endif
  for(i=1;i<l;i++) r*=3;
#ifdef DEBUG
  printf("ddt_fun : level=%i step=%i\n",l,r);
#endif
  c = *s;
  for(i=0;i<3*r;i++) if(c!=s[i]) t=0;
  if(t && c!='-' && !isdigit(c)) f=c;
  if(f!=c && c!='-' && !isdigit(c))
  {
    t = 1;
    c = s[0];
    for(i=0;i<r;i++) if(c!=s[i]) t=0;
    if(t)
    {
      s3[0] = (char)c;
      c = s[r];
      for(i=0;i<r;i++) if(c!=s[i+r]) t=0;
      if(t)
      {
        s3[1] = (char)c;
        c = s[r+r];
        for(i=0;i<r;i++) if(c!=s[i+r+r]) t=0;
        if(t)
        {
          s3[2] = (char)c;
          s3[3] = 0;
          f = ddt_func(s3);
        }
      }
    }
  }
  if(f!=DDT_MUX)
  {
#ifdef DEBUG
    printf("ddt_fun : special case %i [%s]\n",f,s3);
#endif
  }
  else if(!strncmp(s,&s[r],(size_t)r) && !strncmp(&s[r],&s[r<<1],(size_t)r)) f=-1;
  else if( strncmp(s,&s[r],(size_t)r) && !strncmp(&s[r],&s[r<<1],(size_t)r)) f=DDT_E12;
  else if(!strncmp(s,&s[r],(size_t)r) &&  strncmp(&s[r],&s[r<<1],(size_t)r)) f=DDT_E21;
#ifdef DEBUG
  printf("ddt_fun -> %i\n",f);
#endif
  return f;
}

/* Convert part of preprocessed string to C code portion */

char* ddt_toc(char *s, int r)
{
  int k;
  char t[8];
  static char v[8];
#ifdef DEBUG
  printf("ddt_toc [%s] %i\n",s,r);
#endif
  *v = 0;
  if(r>7) r=7;
  strncpy(t,s,(size_t)r);
  t[r] = 0;
  if(*t=='N') strcpy(v,"N");
  if(*t=='O') strcpy(v,"O");
  if(*t=='P') strcpy(v,"P");
  if(isdigit(*t)||*t=='-') 
  {
    k = atoi(t);
    if(k < 0) sprintf(v,"i%i",-k);
    else sprintf(v,"r%i",k);
  }
#ifdef DEBUG
  printf("ddt_toc -> %s\n",v);
#endif
  return v;
}


/* Convert string with ternary number to integer */

int ddt_ternary(char *s)
{
 int a,t,j,l;
 a = 0;
 t = 1;
 l = (int)strlen(s);
 for(j=l-1;j>=0;j--)
 {
   switch(s[j])
   {
     case 'N':
     case 'n':
     case '2':
     case '-':
       a -= t;
       break;
     case 'O':
     case 'o':
     case '0':
       break;
     case 'P':
     case 'p':
     case '1':
     case '+':
       a += t;
       break;
     default:
       printf("ERROR: Wrong ternary string '%s'!\n",s); 
   }
   t *= 3;
 }
#ifdef DEBUG
 printf("ddt_ternary(%s) -> %i\n",s,a);
#endif
 return a;
}

/* Parse ddt-file and return allocated string with function signature */

char* ddt_parse(char *file)
{
  FILE *f;
  char *r = NULL;
  char *po,s[100];
  int j,k,x,w,a,sz;
  int *id = NULL;
#ifdef DEBUG
  int i = 0;
  printf("ddt_parse %s\n",file);
#endif
  f = fopen(file,"rt");
  if(f==NULL) return NULL;
  while(1)
  {
    fgets(s,100,f);
    if(feof(f)) break;
    po = strrchr(s,'\n');
    if(po!=NULL) *po=0;
    po = strrchr(s,'\r');
    if(po!=NULL) *po=0;
    if(!*s) continue;
    if(s[0]=='#') continue;
    if(s[0]=='/' && s[1]=='/') continue;
    po = NULL;
#ifdef DEBUG
    printf("ddt_parse : %i) %s\n",++i,s);
#endif
    if(r==NULL)
    {
      po = strchr(s,'=');
      if(po==NULL) 
      {
        printf("ERROR: Wrong file format!\n");
        break;
      }
      *po=0;po++;
      ddt_ins = (int)strlen(s);
      ddt_outs = (int)strlen(po);
      id = (int*)malloc((unsigned int)ddt_ins*sizeof(int));
      if(id==NULL)
      {
        printf("ERROR: Out of limits!\n");
        break;
      }
      k = 1;
      for(j=0;j<ddt_ins;j++) k*=3;
      sz = (k+1)*ddt_outs;
      r = (char*)malloc((unsigned int)sz);
      if(r==NULL) break;
      memset(r,'X',(size_t)(sz-1));
      r[sz-1] = 0;
      for(j=0;j<ddt_outs-1;j++) r[k+j*k+j]=',';
#ifdef DEBUG
      printf("ddt_parse i=%i o=%i k=%i sz=%i\n",ddt_ins,ddt_outs,k,sz);
#endif
    }
    if(po==NULL)
    {
      po = strchr(s,'=');
      if(po==NULL) 
      {
        printf("ERROR: Wrong file format!\n");
        break;
      }
      *po=0;po++;
    }
    if((int)strlen(s)!=ddt_ins)
    {
      free(r);
      r = NULL;
      printf("ERROR: Wrong file format!\n");
      break;
    }
    x = 0;
    for(j=ddt_ins-1;j>=0;j--) if(s[j]=='X') id[x++]=j;
    if(x==0)
    {
      a = ddt_ternary(s);
      for(j=0;j<ddt_outs;j++) r[a+(k>>1)+j*(k+1)]=po[ddt_outs-j-1];
    }
    else
    {
      for(j=0;j<ddt_ins;j++) if(s[j]=='X') s[j]='n';
      w = 1;
      while(w)
      {
        a = ddt_ternary(s);
        for(j=0;j<ddt_outs;j++) r[a+(k>>1)+j*(k+1)]=po[ddt_outs-j-1];
        a = 0;
        w = 0;
        while(a < x)
        {
          switch(s[id[a]])
          {
            case 'n': 
              s[id[a]] = 'o';
              a = x;
              w = 1;
              break;
            case 'o':
              s[id[a]] = 'p';
              a = x;
              w = 1;
              break;
            case 'p':
              s[id[a]] = 'n';
              a++;
              break;  
          }
        }
      }
    }
  }
  fclose(f);
  if(id!=NULL) free(id);
#ifdef DEBUG
  printf("ddt_parse -> %i '%s'\n",sz,r?r:"NULL");
#endif
  return r;
}

/* Main function */

int main(int argc, char **argv)
{
  FILE *f;
  Line *l;
  int curf=0;
  int nok=0,i,j,k,n,r,e12,e21;
  char *sign,*po,*s,s3[4],func[16],fname[32]="ddt_",*str=NULL;
  printf(DDT_COPYR "DDTc is decision diagrams for ternary synthesis tool and translator to C\n\n");
  if(argc < 2)
  {
    printf("Usage:\n\tddtc signature [funcname]\n\tddtc file.ddt\n\n");
    printf("Examples:\n\n\tddtc NNNNOONOP min\n\tddtc NOP,PON\n\n");
    return -1;
  }
  if(strstr(argv[1],".ddt")!=NULL)
  {
    strcat(fname,argv[1]);
    s = strchr(fname,'.');
    if(s!=NULL) *s=0;
    sign = ddt_parse(argv[1]);
    if(sign==NULL) return -2;
    if(argc>2 && !strcmp(argv[2],"-e")) ddt_eflag=1;
  }
  else
  {
    sign = (char*)malloc(strlen(argv[1])+1);
    if(sign==NULL) return -2;
    strcpy(sign,argv[1]);
    if(argc>2) 
    {
      if(*argv[2]!='-') strcat(fname,argv[2]);
      else if(!strcmp(argv[2],"-e")) ddt_eflag=1;
    }
    if(argc>3 && !strcmp(argv[3],"-e")) ddt_eflag=1;
  }
  for(i=0;i<(int)strlen(fname);i++) fname[i]=(char)tolower(fname[i]);
  strcpy(func,fname);
  strcat(fname,".c");
  s = sign;
  k = (int)strlen(sign);
#ifdef DEBUG
  printf("%i'%s'\n",k,s);
#endif
  ddt_outs = 1;
  for(i=0;i<k;i++)
  {
    if(sign[i]==',') ddt_outs++;
  }
  ddt_sign = (ddt_struct*)malloc((unsigned int)ddt_outs*sizeof(ddt_struct));
  if(ddt_sign==NULL)
  {
    nok = -3;
    printf("Out of memory!\n");
  }
  j = 0;
  for(i=0;i<k;i++)
  {
    if(sign[i]==',')
    {
      sign[i] = 0;
      ddt_sign[j].s = ddt_new(s);
      ddt_sign[j].a = ddt_calc(ddt_sign[j].s);
      j++;
      s = &sign[i+1];
    }
  }
  ddt_sign[j].s = ddt_new(s);
  ddt_sign[j].a = ddt_calc(ddt_sign[j].s);
  free(sign);
  ddt_ins = ddt_sign[0].a;
  printf("%i inputs\n%i outputs\n",ddt_ins,ddt_outs);
  if(ddt_ins<=0 || j+1!=ddt_outs)
  { 
    printf("Wrong signature '%s'\n",argv[1]);
    ddt_ins = 0;
    nok = -4;
  }
  else
  {
    for(i=0;i<ddt_outs;i++)
    {
      if(ddt_sign[i].a!=ddt_ins || ddt_sign[i].s==NULL)
      {
        printf("Wrong signature %s (%i), but %i\n",ddt_sign[i].s,ddt_sign[i].a,ddt_ins);
        ddt_ins = 0;
        nok = -4;
        break;
      }
    }
  }
  if(!nok && ddt_ins > 0 && ddt_outs > 0)
  {
    j = 1;
    for(i=0;i<ddt_ins;i++) j*=3;
    if(++j<1000) j=1000;
#ifdef DEBUG
    printf("size=%i\n",j);
#endif
    str = (char*)malloc((unsigned int)j);
    if(str==NULL)
    {
      nok = -3;
      printf("Out of memory!\n");
    }
    else
    {
      funs = TextNew();
      if(funs==NULL) nok=-3;
    }
    if(funs!=NULL)
    {
      for(i=0;i<ddt_outs;i++)
      {
        printf("\nOutput %i:\n",i+1);
        printf("level 1\n");
        j = 0;
        while(ddt_sign[i].s[j])
        {
          po = &ddt_sign[i].s[j];
          k = ddt_func(po);
          strncpy(s3,po,3);
          s3[3] = 0;
          switch(k)
          {
            case 'N':
            case 'O':
            case 'P':
              break;
            case 0: 
              po[0] = '-';
              po[1] = '0';
              po[2] = '1';
              break;
            case DDT_INV:
            case DDT_BLP:
            case DDT_BLN:
            case DDT_ROU:
            case DDT_ROD:
            case DDT_SHU:
            case DDT_SHD:
            case DDT_PTI:
            case DDT_NTI:
            case DDT_PHI:
            case DDT_NHI:
            case DDT_E12:
            case DDT_E21:
            case DDT_MUX:
              l = TextFindFirstStr(funs,s3);
              if(l==NULL)
              {
                l = TextAdd(funs,s3);
                l->type = k;
                l->id = 1;
                l->adr = (unsigned short)(++curf);
                if(curf > 999) nok=-5;
              }
              else
              {
                if(l->id!=1) printf("LEVEL ERROR!\n");
              }
              if(l==NULL) nok=-6;
              if(!nok) 
              {
                sprintf(s3,"%03d",l->adr);
                po[0] = s3[0];
                po[1] = s3[1];
                po[2] = s3[2];
              }
              break;
            default:
              nok=-7;
          }
          if(nok) break;
          j+=3;
        }
        if(nok) break;
#ifdef DEBUG
        printf("[%s]\n",ddt_sign[i].s);
#endif
        r = 9;
        for(n=2;n<=ddt_ins;n++)
        {
          printf("level %i\n",n);
          j = 0;
          while(ddt_sign[i].s[j])
          {
            po = &ddt_sign[i].s[j];
            k = ddt_fun(n,po);
            switch(k)
            {
              case 'N':
              case 'O':
              case 'P':
                break;
              case -1:
                memset(&po[r/3],' ',(size_t)(2*r/3));
                break;
              case 0:
                memset(po,' ',(size_t)r);
                sprintf(po,"%d",-n);
                po[strlen(po)] = ' ';
                break;
              case DDT_INV:
              case DDT_BLP:
              case DDT_BLN:
              case DDT_ROU:
              case DDT_ROD:
              case DDT_SHU:
              case DDT_SHD:
              case DDT_PTI:
              case DDT_NTI:
              case DDT_PHI:
              case DDT_NHI:
              case DDT_E12:
              case DDT_E21:
              case DDT_MUX:
                memcpy(str,po,(size_t)r);
                str[r] = 0;
                l = TextFindFirstStr(funs,str);
                if(l==NULL)
                {
                  l = TextAdd(funs,str);
                  l->type = k;
                  l->id = n;
                  l->adr = (unsigned short)(++curf);
                }
                else
                {
                  if(n!=l->id) printf("LEVEL ERROR!\n");
                }
                memset(po,' ',(size_t)r);
                sprintf(po,"%06d",l->adr);
                po[6] = ' ';
                break;
              default:
                nok=-8;
            }
            if(nok) break;
            j += r;
          }
          if(nok) break;
          r *= 3;
#ifdef DEBUG
          printf("[%s]\n",ddt_sign[i].s);
#endif
        }
      }
    }
  }
  if(!nok)
  {
    f = fopen(fname,"wt");
    if(f!=NULL)
    {
      e12 = 0;
      e21 = 0;
      for(l=funs->first;l!=NULL;l=l->next)
      {
          switch(l->type)
          {
              case DDT_MUX: e12++;e21++;break;
              case DDT_E12: e12++;break;
              case DDT_E21: e21++;break;
              case DDT_INV: e12++;e21++;break;
              case DDT_BLP: e12++;break;
              case DDT_BLN: e21++;break;
              case DDT_ROU: e12++;e21++;break;
              case DDT_ROD: e12++;e21++;break;
              case DDT_SHU: e12++;break;
              case DDT_SHD: e21++;break;
              case DDT_PTI: e21++;break;
              case DDT_NTI: e12++;break;
              case DDT_PHI: e21++;break;
              case DDT_NHI: e12++;break;
          }
      }
      if(e12&1) e12++;
      if(e21&1) e21++;
      printf("\nGenerate file %s with %i functions (%i x DG403)\n",fname,curf,(e12+e21)>>1);
      fprintf(f,"/* Generated by DDTc v" DDT_VERSION "\n   Solution is %i x DG403\n   See www.ternary.info */\n",(e12+e21)>>1);
      fprintf(f,"\n#include \"ddt.h\"\n");
      fprintf(f,"\nint %s(int f",func);
      for(i=0;i<ddt_ins;i++) 
         fprintf(f,", DDT i%i",i+1);
      for(i=0;i<ddt_outs;i++) 
         fprintf(f,", DDT* o%i",i+1);
      fprintf(f,")\n{\n");
      if(curf)
      {
        fprintf(f," DDT ");
        for(i=1;i<=curf;i++) 
        {
          if(i!=1) fputc(',',f);
          fprintf(f,"r%i",i);
        }
        fprintf(f,";\n int ");
        for(i=1;i<=curf;i++) 
        {
          if(i!=1) fputc(',',f);
          fprintf(f,"f%i",i);
        }
        fprintf(f,";\n");
      }
      for(l=funs->first;l!=NULL;l=l->next)
      {
          r = 1;
          for(i=1;i<l->id;i++) r*=3;
          switch(l->type)
          {
            case DDT_INV:
              fprintf(f," f%i = ddt_inv(f,i%i,&r%i);\n",l->adr,l->id,l->adr);
              break;
            case DDT_BLP:
              fprintf(f," f%i = ddt_blp(f,i%i,&r%i);\n",l->adr,l->id,l->adr);
              break;
            case DDT_BLN:
              fprintf(f," f%i = ddt_bln(f,i%i,&r%i);\n",l->adr,l->id,l->adr);
              break;
            case DDT_ROU:
              fprintf(f," f%i = ddt_rou(f,i%i,&r%i);\n",l->adr,l->id,l->adr);
              break;
            case DDT_ROD:
              fprintf(f," f%i = ddt_rod(f,i%i,&r%i);\n",l->adr,l->id,l->adr);
              break;
            case DDT_SHU:
              fprintf(f," f%i = ddt_shu(f,i%i,&r%i);\n",l->adr,l->id,l->adr);
              break;
            case DDT_SHD:
              fprintf(f," f%i = ddt_shd(f,i%i,&r%i);\n",l->adr,l->id,l->adr);
              break;
            case DDT_PTI:
              fprintf(f," f%i = ddt_pti(f,i%i,&r%i);\n",l->adr,l->id,l->adr);
              break;
            case DDT_NTI:
              fprintf(f," f%i = ddt_nti(f,i%i,&r%i);\n",l->adr,l->id,l->adr);
              break;
            case DDT_PHI:
              fprintf(f," f%i = ddt_phi(f,i%i,&r%i);\n",l->adr,l->id,l->adr);
              break;
            case DDT_NHI:
              fprintf(f," f%i = ddt_nhi(f,i%i,&r%i);\n",l->adr,l->id,l->adr);
              break;
            case DDT_E12:
              fprintf(f," f%i = ddt_e12(f,i%i",l->adr,l->id);
              fprintf(f,",%s",ddt_toc(l->str,r));
              fprintf(f,",%s",ddt_toc(&l->str[r],r));
              fprintf(f,",&r%i);\n",l->adr);
              break;
            case DDT_E21:
              fprintf(f," f%i = ddt_e21(f,i%i",l->adr,l->id);
              fprintf(f,",%s",ddt_toc(l->str,r));
              fprintf(f,",%s",ddt_toc(&l->str[r<<1],r));
              fprintf(f,",&r%i);\n",l->adr);
              break;
            case DDT_MUX:
              fprintf(f," f%i = ddt_mux(f,i%i",l->adr,l->id);
              fprintf(f,",%s",ddt_toc(l->str,r));
              fprintf(f,",%s",ddt_toc(&l->str[r],r));
              fprintf(f,",%s",ddt_toc(&l->str[r<<1],r));
              fprintf(f,",&r%i);\n",l->adr);
              break;
            default:
              nok=-9;
          }
          fprintf(f," if(f%i < 0) return f%i;\n",l->adr,l->adr);
      }
      for(i=0;i<ddt_outs;i++)
      {
        fprintf(f," if(o%i) *o%i = %s;\n",i+1,i+1,ddt_toc(ddt_sign[i].s,9));
      }
      fprintf(f," return ");
      if(curf)
      {
        for(i=1;i<=curf;i++) 
        {
          if(i!=1) fputc('+',f);
          fprintf(f,"f%i",i);
        }
        fprintf(f,";\n");
      }
      else fprintf(f,"0;\n");
      fprintf(f,"}\n");
      fclose(f);
    }
    else nok=-10;
  }
  for(i=0;i<ddt_outs;i++)
  {
    if(ddt_sign[i].s!=NULL) 
    {
       free(ddt_sign[i].s);
    }
  }
  free(ddt_sign);
  free(str);
  if(funs!=NULL)
  {
#ifdef DEBUG
     TextList(funs);
#endif
     TextDel(funs);
  }
  if(nok)
    printf("ERROR%i\n\n",nok);
  else
    printf("OK\n\n");
  return nok;
}
