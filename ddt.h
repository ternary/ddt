/*  ddt.h - decision diagrams for ternary synthesis and simulation

    Copyright (C) 2010, Alexander A. Shabarshin <me@shaos.net>
    Copyright (C) 2015, TERNARY RESEARCH CORPORATION <ddt@ternary.info>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __DDT_H
#define __DDT_H

#define DDT_VERSION "0.5"

#define DDT_COPYR "\nDDT v" DDT_VERSION " (C) 2010 Alexander A. Shabarshin <me@shaos.net>\n\t (C) 2015 TERNARY RESEARCH CORPORATION <ddt@ternary.info>\n\n"

typedef int DDT; /* Possible values: N,O,P */

#define N 'N'
#define O 'O'
#define P 'P'

#define DDT_SIM 0 /* Simulate and return a number of blocks (>0) or an error (<0) */
#define DDT_E12 1 /* Ternary element XYY */
#define DDT_E21 2 /* Ternary element XXY */
#define DDT_MEM 3 /* Sample and hold circuit with 2 inputs */
#define DDT_MUX 100 /* Universal ternary element */
#define DDT_BUF 101 /* Ternary buffer */
#define DDT_INV 102 /* Ternary inverter (negation) */
#define DDT_BLP 103 /* Block positive */
#define DDT_BLN 104 /* Block negative */
#define DDT_ROU 105 /* Rotate up */
#define DDT_ROD 106 /* Rotate down */
#define DDT_SHU 107 /* Shift up */
#define DDT_SHD 108 /* Shift down */
#define DDT_PTI 109 /* Positive ternary inverter */
#define DDT_NTI 110 /* Negative ternary inverter */
#define DDT_PHI 111 /* Positive half-inverter */
#define DDT_NHI 112 /* Negative half-inverter */
#define DDT_MIN 200 /* Minimum */
#define DDT_MAX 201 /* Maximum */

/*
 all ternary functions of one argument:
 NNN = N
 NNO = SHD(x) = E21(x,N,O)
 NNP = E21(x,N,P)
 NON = MUX(x,N,O,N) = E12(x,N,E21(x,O,N))
 NOO = BLP(x) = E12(x,N,O) // analog of reverse diode
 NOP = x // BUF(x) or buffer MUX(x,N,O,P) = E12(x,N,E21(x,O,P))
 NPN = MUX(x,N,P,N) = E12(x,N,E21(x,P,N))
 NPO = MUX(x,N,P,O) = E12(x,N,E21(x,P,O))
 NPP = E12(x,N,P)
 ONN = NHI(x) = E12(x,O,N)
 ONO = MUX(x,O,N,O) = E12(x,O,E21(x,N,O))
 ONP = MUX(x,O,N,P) = E12(x,O,E21(x,N,P))
 OON = E21(x,O,N)
 OOO = O
 OOP = BLN(x) = E21(x,O,P) // analog of forward diode
 OPN = ROU(x) = MUX(x,O,P,N) = E12(x,O,E21(x,P,N))
 OPO = MUX(x,O,P,O) = E12(x,O,E21(x,P,O))
 OPP = SHU(x) = E12(x,O,P)
 PNN = NTI(x) = E12(x,P,N)
 PNO = ROD(x) = MUX(x,P,N,O) = E12(x,P,E21(x,N,O))
 PNP = MUX(x,P,N,P) = E12(x,P,E21(x,N,P))
 PON = INV(x) = MUX(x,P,O,N) = E12(x,P,E21(x,O,N))
 POO = E12(x,P,O)
 POP = MUX(x,P,O,P) = E12(x,P,E21(x,O,P))
 PPN = PTI(x) = E21(x,P,N)
 PPO = PHI(x) = E21(x,P,O)
 PPP = P
*/

#define DDT_ERROR_ARG -1 /* wrong argument values */
#define DDT_ERROR_EXC -2 /* exception during evaluation */
#define DDT_ERROR_MEM -3 /* out of memory */

#ifdef __cplusplus
extern "C" {
#endif
int ddt_e12(int f, DDT c, DDT a, DDT b, DDT* r);
int ddt_e21(int f, DDT c, DDT a, DDT b, DDT* r);
int ddt_mem(int f, DDT c, DDT a, DDT b, DDT* r, int i);
int ddt_mux(int f, DDT c, DDT n, DDT o, DDT p, DDT *r);
int ddt_buf(int f, DDT a, DDT* r);
int ddt_inv(int f, DDT a, DDT* r);
int ddt_blp(int f, DDT a, DDT* r);
int ddt_bln(int f, DDT a, DDT* r);
int ddt_rou(int f, DDT a, DDT* r);
int ddt_rod(int f, DDT a, DDT* r);
int ddt_shu(int f, DDT a, DDT* r);
int ddt_shd(int f, DDT a, DDT* r);
int ddt_pti(int f, DDT a, DDT* r);
int ddt_nti(int f, DDT a, DDT* r);
int ddt_phi(int f, DDT a, DDT* r);
int ddt_nhi(int f, DDT a, DDT* r);
int ddt_min(int f, DDT a, DDT b, DDT* r);
int ddt_max(int f, DDT a, DDT b, DDT* r);
#ifdef __cplusplus
}
#endif

#endif
